'''
Hamed Ghamari
hgg3ds@gmail.com
'''

import maya.cmds as cmds
import os

def antivirus():
    jobs = cmds.scriptJob(lj=True)
    for job in jobs:
        if "leukocyte.antivirus()" in job:
            id = job.split(":")[0]
            if id.isdigit():
                cmds.scriptJob(k=int(id), f=True)

    breed_genes = cmds.ls('breed_gene*')
    for item in breed_genes:
        try:
            cmds.delete(item)
        except:
            cmds.warning("The \"%s\" node cannot be deleted, are you sure its not a refrenced node?"%item)
        

    vaccine_genes = cmds.ls('vaccine_gene*')
    for item in vaccine_genes:
        try:
            cmds.delete(item)
        except:
            cmds.warning("The \"%s\" node cannot be deleted, are you sure its not a refrenced node?"%item)
        
    scriptPath = os.path.join(cmds.internalVar (userAppDir=1), 'scripts')
    if os.path.exists(os.path.join(scriptPath,'vaccine.py')):
        os.remove(os.path.join(scriptPath,'vaccine.py'))
    if os.path.exists(os.path.join(scriptPath,'vaccine.pyc')):
        os.remove(os.path.join(scriptPath,'vaccine.pyc'))


antivirus()
jobNum = cmds.scriptJob( e= ["SceneOpened","antivirus()"], protected=True)
